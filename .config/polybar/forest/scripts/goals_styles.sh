#!/usr/bin/env bash

# Color files
PFILE="$HOME/.config/polybar/forest/module.ini"
RFILE="$HOME/.config/polybar/forest/scripts/rofi/colors.rasi"

# Change colors
change_color() {
	# polybar
	# sed -i -e "s/background = #.*/background = $BG/g" $PFILE
	# sed -i -e "s/foreground = #.*/foreground = $FG/g" $PFILE
	# sed -i -e "s/sep = #.*/sep = $SEP/g" $PFILE

	polybar-msg cmd restart
}

if  [[ $1 = "--default" ]]; then
	FORMAT_PREFIX_FG="#4f0707"
	FORMAT_BG="#212B30"
	FORMAT_FG="#C4C7C5"
	change_color
elif  [[ $1 = "--goals_42" ]]; then
	BG="#0e3066"
	FG="#FFFFFF"
	BGA="#292030"
	SEP="#473F4E"
	AC="#D94084"
	SE="#4F5D95"
	change_color
elif  [[ $1 = "--goals_health" ]]; then
	BG="#4f0707"
	FG="#FFFFFF"
	BGA="#292030"
	SEP="#473F4E"
	AC="#D94084"
	SE="#4F5D95"
	change_color
elif  [[ $1 = "--goals_social" ]]; then
	BG="#046b04"
	FG="#FFFFFF"
	BGA="#292030"
	SEP="#473F4E"
	AC="#D94084"
	SE="#4F5D95"
	change_color
elif  [[ $1 = "--goals_cooking" ]]; then
	BG="#8c9c03"
	FG="#FFFFFF"
	BGA="#292030"
	SEP="#473F4E"
	AC="#D94084"
	SE="#4F5D95"
	change_color
else
	cat <<- _EOF_
	No option specified, Available options:
	--default    --goals_42    --goals_health    --goals_social    --goals_cooking
	_EOF_
fi
