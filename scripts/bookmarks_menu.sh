# rofi -no-config -no-lazy-grab -show bookmarks -modi 'bookmarks: /home/kryckely/scripts/rofi-bookmarks.py' -theme ~/.config/polybar/forest/scripts/rofi/launcher.rasi
# rofi -show bookmarks -modi 'bookmarks: /home/kryckely/scripts/rofi-bookmarks/rofi-bookmarks.py'

#!/usr/bin/env bash

# Available Styles
# >> Created and tested on : rofi 1.6.0-1
#
# blurry	blurry_full		kde_simplemenu		kde_krunner		launchpad
# gnome_do	slingshot		appdrawer			appdrawer_alt	appfolder
# column	row				row_center			screen			row_dock		row_dropdown

theme="blurry"
# theme="gnome_do"
dir="$HOME/.config/rofi/launchers/misc"

# comment these lines to disable random style
#themes=($(ls -p --hide="launcher.sh" $dir))
#theme="${themes[$(( $RANDOM % 16 ))]}"

rofi -no-lazy-grab -show bookmarks -theme $dir/"$theme" -modi "bookmarks: /home/kryckely/scripts/rofi-bookmarks.py menu --profile Home %u"
i3-msg '[class="Firefox"] focus'
