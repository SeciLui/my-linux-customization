#!/usr/bin/env python3

import i3ipc
import sys
import time

i3 = i3ipc.Connection()
is_window = True

while (is_window == True):
    is_window = False
    tree = i3.get_tree()
    workspaces = tree.workspaces()
    for workspace in workspaces:
        print("ws : ", workspace.name)
        if (workspace.name == sys.argv[1]):
            is_window = True
            for window in workspace:
                print("window :", window.name)
                i3.command('[id=' + str(window.window) +'] kill')
                time.sleep(0.2)
