#!/usr/bin/env bash

# Available Styles
# >> Created and tested on : rofi 1.6.0-1
#
# style_1     style_2     style_3     style_4     style_5     style_6     style_7

theme="style_1"

dir="$HOME/.config/rofi/launchers/text"

# comment these lines to disable random style
#themes=($(ls -p --hide="launcher.sh" $dir))
#theme="${themes[$(( $RANDOM % 16 ))]}"

# rofi -no-lazy-grab -modi :/home/kryckely/scripts/rofi-taskwarrior_goals -show  -theme $dir/"$theme"

rofi -no-lazy-grab -modi :/home/kryckely/scripts/rofi-taskwarrior_life-goals_repo/target/release/rofi-taskwarrior -show  -theme $dir/"$theme"
