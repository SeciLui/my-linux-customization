#!/usr/bin/env python3
import subprocess
import time
import sys
import re

notif_interval_sec = int(sys.argv[1])
notified = False

def get(command):
    return subprocess.check_output(command).decode("utf-8")

def is_current_event(t_curr, t_event_start, t_event_end):
    if (t_event_start > t_curr):
        return (False)
    elif (t_event_end < t_curr):
        return (False)
    return (True)

def notify_if_necessary(t_curr, t_event_start, event_name):
    global notified
    if (t_event_start < t_curr):
        return
    t_remaining = t_event_start - t_curr
    if (t_remaining < 600):
        subprocess.run(["notify-send", "-i", "/home/kryckely/scripts/fontawesome-pro/svgs/light/alarm-clock.svg", "-u", "critical", "-t", str(notif_interval_sec * 1000), event_name])
        # subprocess.call(["paplay ", "/usr/share/sounds/freedesktop/stereo/message.oga"])
        notified = True
    elif (t_remaining < 900):
        subprocess.run(["notify-send", "-i", "/home/kryckely/scripts/fontawesome-pro/svgs/light/alarm-clock.svg", "-u", "normal", "-t", "10000", event_name])
        # subprocess.call(["paplay ", "/usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga"])
        notified = True

last_notif_sec = 0

while True :
    currtime_sec = time.time()
    calcurse_output = [l.strip() for l in get(["calcurse", "-D", "/sync/Sync/Home/.profile/calcurse_data", "-a", "--format-apt", "- %s -> %e\\nNOTE:%N\\n%m\\n", "--format-recur-apt", "- %s -> %e\\nNOTE:%N\\n%m\\n"]).splitlines()][1:]
    # arrange event data:
    parsed_events = []; shedule = []
    for line in calcurse_output:
        if (line == ""):
            continue
        elif line.startswith("NOTE:"):
            note = line.replace("NOTE:", "").replace("\t", "")
        elif line.startswith("- "):
            shedule.append(int(line.split("->")[0].replace("-", "").strip()))
            shedule.append(int(line.split("->")[1].strip()))
        else:
            parsed_events.append([shedule] + [line] + [note])
            shedule = []

    current_event_found = False
    notif_send = False
    for event in parsed_events:
        if (is_current_event(currtime_sec, event[0][0], event[0][1]) == True):
            remaining_time = event[0][1] - currtime_sec
            print(event[1], time.strftime("[%H:%M]", time.gmtime(remaining_time)), sep=' ', end='\n', file=sys.stdout)
            sys.stdout.flush()
            profile = re.search('\[(.*)\]', event[2])
            if (profile is not None):
                subprocess.run(["/home/kryckely/scripts/profile_switch.sh", "--" + profile.group(1)])
            current_event_found = True
        if (currtime_sec - last_notif_sec >= notif_interval_sec):
            notif_send = True
            notify_if_necessary(currtime_sec, event[0][0], event[1])
    if (notif_send == True and notified == True):
        last_notif_sec = time.time()
        notified = False
    if (current_event_found == False):
        print(" ", file=sys.stdout)
        sys.stdout.flush()
    time.sleep(10)
