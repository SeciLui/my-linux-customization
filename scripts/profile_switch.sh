#!/usr/bin/env bash

argv="$1";
shift;              #expose next argument
case "$argv" in
	"--sport_mental" )
		if [ "$(cat /home/kryckely/.current_profile)" != " Sport & Mental" ]
		then
			/home/kryckely/scripts/close_workspace.py 2
			/home/kryckely/scripts/close_workspace.py 3
			/home/kryckely/scripts/close_workspace.py 5
			find /home/kryckely/Documents -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/Sport\ \&\ Mental/* /home/kryckely/Documents
			echo "yes" | task config data.location /sync/Sync/Sport\ \&\ Mental/.profile/taskwarrior_data
			sed -i 's/--profile.*$/--profile \/sync\/Sync\/Sport\\\ \\\&\\\ Mental\/.profile\/joplin_data/g' /home/kryckely/.local/share/applications/appimagekit-joplin.desktop
			find /home/kryckely/.config/fish -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/Sport\ \&\ Mental/.profile/fish/* /home/kryckely/.config/fish/
			sed -i 's/firefox -P.*$/firefox -P \"Sport \& Mental\" %u/g' /home/kryckely/.local/share/applications/firefox.desktop
			sed -i 's/--profile.*$/--profile \\\"Sport \& Mental\\\" %u\"/g' /home/kryckely/scripts/bookmarks_menu.sh
			# sed -i 's/firefox -P.*$/firefox -P \\\"Sport \& Mental\\\"\"/g' /home/kryckely/scripts/quicklinks.sh
			### Changement du treemacs-persist ###
			find /home/kryckely/.emacs.d/.cache -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/Sport\ \&\ Mental/.profile/emacs_data/treemacs-persist /home/kryckely/.emacs.d/.cache/
			### Notification et changement du profile ###
			echo " Sport & Mental" > /home/kryckely/.current_profile
			notify-send -i /home/kryckely/scripts/fontawesome-pro/svgs/light/user-circle.svg -u normal -t 5000 "Profile" "Successfully changed to \" Sport & Mental\""
		fi

		;;
	"--cooking_nutrition" )
		if [ "$(cat /home/kryckely/.current_profile)" != " Cooking & Nutrition" ]
		then
			/home/kryckely/scripts/close_workspace.py 2
			/home/kryckely/scripts/close_workspace.py 3
			/home/kryckely/scripts/close_workspace.py 5
			find /home/kryckely/Documents -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/Cooking\ \&\ Nutrition/* /home/kryckely/Documents
			echo "yes" | task config data.location /sync/Sync/Cooking\ \&\ Nutrition/.profile/taskwarrior_data
			sed -i 's/--profile.*$/--profile \/sync\/Sync\/Cooking\\\ \\\&\\\ Nutrition\/.profile\/joplin_data/g' /home/kryckely/.local/share/applications/appimagekit-joplin.desktop
			find /home/kryckely/.config/fish -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/Cooking\ \&\ Nutrition/.profile/fish/* /home/kryckely/.config/fish/
			sed -i 's/firefox -P.*$/firefox -P \"Cooking \& Nutrition\" %u/g' /home/kryckely/.local/share/applications/firefox.desktop
			sed -i 's/--profile.*$/--profile \\\"Cooking \& Nutrition\\\" %u\"/g' /home/kryckely/scripts/bookmarks_menu.sh
			# sed -i 's/firefox -P.*$/firefox -P \\\"Cooking \& Nutrition\\\"\"/g' /home/kryckely/scripts/quicklinks.sh
			### Changement du treemacs-persist ###
			find /home/kryckely/.emacs.d/.cache -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/Cooking\ \&\ Nutrition/.profile/emacs_data/treemacs-persist /home/kryckely/.emacs.d/.cache/
			### Notification et changement du profile ###
			echo " Cooking & Nutrition" > /home/kryckely/.current_profile
			notify-send -i /home/kryckely/scripts/fontawesome-pro/svgs/light/user-circle.svg -u normal -t 5000 "Profile" "Successfully changed to \" Cooking & Nutrition\""
		fi

		;;
	"--socialskills_behave" )
		if [ "$(cat /home/kryckely/.current_profile)" != " SocialSkills & Behave" ]
		then
			/home/kryckely/scripts/close_workspace.py 2
			/home/kryckely/scripts/close_workspace.py 3
			/home/kryckely/scripts/close_workspace.py 5
			find /home/kryckely/Documents -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/SocialSkills\ \&\ Behave/* /home/kryckely/Documents
			echo "yes" | task config data.location /sync/Sync/SocialSkills\ \&\ Behave/.profile/taskwarrior_data
			sed -i 's/--profile.*$/--profile \/sync\/Sync\/SocialSkills\\\ \\\&\\\ Behave\/.profile\/joplin_data/g' /home/kryckely/.local/share/applications/appimagekit-joplin.desktop
			find /home/kryckely/.config/fish -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/SocialSkills\ \&\ Behave/.profile/fish/* /home/kryckely/.config/fish/
			sed -i 's/firefox -P.*$/firefox -P \"SocialSkills \& Behave\" %u/g' /home/kryckely/.local/share/applications/firefox.desktop
			sed -i 's/--profile.*$/--profile \\\"SocialSkills \& Behave\\\" %u\"/g' /home/kryckely/scripts/bookmarks_menu.sh
			# sed -i 's/firefox -P.*$/firefox -P \\\"SocialSkills \& Behave\\\"\"/g' /home/kryckely/scripts/quicklinks.sh
			### Changement du treemacs-persist ###
			find /home/kryckely/.emacs.d/.cache -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/SocialSkills\ \&\ Behave/.profile/emacs_data/treemacs-persist /home/kryckely/.emacs.d/.cache/
			### Notification et changement du profile ###
			echo " SocialSkills & Behave" > /home/kryckely/.current_profile
			notify-send -i /home/kryckely/scripts/fontawesome-pro/svgs/light/user-circle.svg -u normal -t 5000 "Profile" "Successfully changed to \" SocialSkills & Behave\""
		fi

		;;
	"--forty_two" )
		if [ "$(cat /home/kryckely/.current_profile)" != " 42" ]
		then
			/home/kryckely/scripts/close_workspace.py 2
			/home/kryckely/scripts/close_workspace.py 3
			/home/kryckely/scripts/close_workspace.py 5
			find /home/kryckely/Documents -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/42/* /home/kryckely/Documents
			echo "yes" | task config data.location /sync/Sync/42/.profile/taskwarrior_data
			sed -i 's/--profile.*$/--profile \/sync\/Sync\/42\/.profile\/joplin_data/g' /home/kryckely/.local/share/applications/appimagekit-joplin.desktop
			find /home/kryckely/.config/fish -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/42/.profile/fish/* /home/kryckely/.config/fish/
			sed -i 's/firefox -P.*$/firefox -P 42 %u/g' /home/kryckely/.local/share/applications/firefox.desktop
			sed -i 's/--profile.*$/--profile 42 %u\"/g' /home/kryckely/scripts/bookmarks_menu.sh
			# sed -i 's/firefox -P.*$/firefox -P 42\"/g' /home/kryckely/scripts/quicklinks.sh
			### Changement du treemacs-persist ###
			find /home/kryckely/.emacs.d/.cache -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/42/.profile/emacs_data/treemacs-persist /home/kryckely/.emacs.d/.cache/
			### Notification et changement du profile ###
			echo " 42" > /home/kryckely/.current_profile
			notify-send -i /home/kryckely/scripts/fontawesome-pro/svgs/light/user-circle.svg -u normal -t 5000 "Profile" "Successfully changed to \" 42\""
		fi

		;;
	"--micro_controller" )
		if [ "$(cat /home/kryckely/.current_profile)" != " MicroController" ]
		then
			/home/kryckely/scripts/close_workspace.py 2
			/home/kryckely/scripts/close_workspace.py 3
			/home/kryckely/scripts/close_workspace.py 5
			find /home/kryckely/Documents -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/MicroController/* /home/kryckely/Documents
			echo "yes" | task config data.location /sync/Sync/MicroController/.profile/taskwarrior_data
			sed -i 's/--profile.*$/--profile \/sync\/Sync\/MicroController\/.profile\/joplin_data/g' /home/kryckely/.local/share/applications/appimagekit-joplin.desktop
			find /home/kryckely/.config/fish -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/MicroController/.profile/fish/* /home/kryckely/.config/fish/
			sed -i 's/firefox -P.*$/firefox -P MicroController %u/g' /home/kryckely/.local/share/applications/firefox.desktop
			sed -i 's/--profile.*$/--profile MicroController %u\"/g' /home/kryckely/scripts/bookmarks_menu.sh
			# sed -i 's/firefox -P.*$/firefox -P MicroController\"/g' /home/kryckely/scripts/quicklinks.sh
			### Changement du treemacs-persist ###
			find /home/kryckely/.emacs.d/.cache -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/MicroController/.profile/emacs_data/treemacs-persist /home/kryckely/.emacs.d/.cache/
			### Notification et changement du profile ###
			echo " MicroController" > /home/kryckely/.current_profile
			notify-send -i /home/kryckely/scripts/fontawesome-pro/svgs/light/user-circle.svg -u normal -t 5000 "Profile" "Successfully changed to \" MicroController\""
		fi

		;;
	"--home" )
		if [ "$(cat /home/kryckely/.current_profile)" != " Home" ]
		then
			/home/kryckely/scripts/close_workspace.py 2
			/home/kryckely/scripts/close_workspace.py 3
			/home/kryckely/scripts/close_workspace.py 5
			find /home/kryckely/Documents -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/Home/* /home/kryckely/Documents
			echo "yes" | task config data.location /sync/Sync/Home/.profile/taskwarrior_data
			sed -i 's/--profile.*$/--profile \/sync\/Sync\/Home\/.profile\/joplin_data/g' /home/kryckely/.local/share/applications/appimagekit-joplin.desktop
			find /home/kryckely/.config/fish -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/Home/.profile/fish/* /home/kryckely/.config/fish/
			sed -i 's/firefox -P.*$/firefox -P Home %u/g' /home/kryckely/.local/share/applications/firefox.desktop
			sed -i 's/--profile.*$/--profile Home %u\"/g' /home/kryckely/scripts/bookmarks_menu.sh
			# sed -i 's/firefox -P.*$/firefox -P Home\"/g' /home/kryckely/scripts/quicklinks.sh
			### Changement du treemacs-persist ###
			find /home/kryckely/.emacs.d/.cache -maxdepth 1 -type l -ls -delete
			ln -s /sync/Sync/Home/.profile/emacs_data/treemacs-persist /home/kryckely/.emacs.d/.cache/
			### Notification et changement du profile ###
			echo " Home" > /home/kryckely/.current_profile
			notify-send -i /home/kryckely/scripts/fontawesome-pro/svgs/light/user-circle.svg -u normal -t 5000 "Profile" "Successfully changed to \" Home\""
		fi

		;;
    *) echo >&2 "Invalid option: $argv"; exit 1;;
esac
