#!/usr/bin/env bash

style="$($HOME/.config/rofi/applets/menu/style.sh)"

dir="$HOME/.config/rofi/applets/menu/configs/$style"
rofi_command="rofi -theme $dir/time.rasi"

LC_TIME=en_US.utf8
## Get time and date
# TIME="$(date +"%I:%M:%S %p")"
DN=$(date +"%A")
MN=$(date +"%B")
DAY="$(date +"%d")"
MONTH="$(date +"%m")"
YEAR="$(date +"%Y")"
HOURS="$(date +"%H")"
MINUTES="$(date +"%M")"
SECONDS="$(date +"%S")"

options="$HOURS\n$MINUTES\n$SECONDS"

## Main
chosen="$(echo -e "$options" | $rofi_command -p "  $DN $DAY $MN ($MONTH)" -dmenu -selected-row 1)"
