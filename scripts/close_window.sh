#!/usr/bin/env bash


argv="$1";
shift;              #expose next argument
case "$argv" in
	"--emacs" )
		while xlsclients | grep -q 'emacs';
		do
			i3-msg '[class="Emacs"] kill';
			sleep 1;
		done

		;;
	"--firefox" )
		while xlsclients | grep -q 'firefox';
		do
			i3-msg '[class="Firefox"] kill';
			sleep 1;
		done

		;;
	"--nautilus" )
		while xlsclients | grep -q 'org.gnome.Nautilus';
		do
			i3-msg '[class="Nautilus"] kill';
			sleep 1;
		done

		;;
    *) echo >&2 "Invalid option: $argv"; exit 1;;
esac
