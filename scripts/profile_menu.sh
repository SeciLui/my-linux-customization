#!/usr/bin/env bash

style="circle"

dir="$HOME/.config/rofi/applets/menu/configs/$style"
rofi_command="rofi -theme $dir/goals_menu.rasi"

# Error msg
msg() {
	rofi -theme "$HOME/.config/rofi/applets/styles/message.rasi" -e "$1"
}

current_goal_state=$(cat /home/kryckely/.current_profile)

# Links
sport_mental=""
cooking_nutrition=""
socialskills_behave=""
forty_two=""
micro_controller=""
home=""

# Variable passed to rofi
options="$sport_mental\n$cooking_nutrition\n$socialskills_behave\n$forty_two\n$micro_controller\n$home"

chosen="$(echo -e "$options" | $rofi_command -p "Current  :  $current_goal_state" -dmenu -selected-row 0)"
case $chosen in
    $sport_mental)
		/home/kryckely/scripts/profile_switch.sh --sport_mental

        ;;
    $cooking_nutrition)
		/home/kryckely/scripts/profile_switch.sh --cooking_nutrition

        ;;
    $socialskills_behave)
		/home/kryckely/scripts/profile_switch.sh --socialskills_behave

        ;;
    $forty_two)
		/home/kryckely/scripts/profile_switch.sh --forty_two

        ;;
    $micro_controller)
		/home/kryckely/scripts/profile_switch.sh --micro_controller

		;;
	$home)
		/home/kryckely/scripts/profile_switch.sh --home

        ;;
esac
